from django.urls import path
from .views import *

app_name = "users"
urlpatterns = [
    # path('', home, name='home'),
    path("register_user/", RegisterView.as_view(), name="register_user"),
    path("send_email/", send_mail, name="send_email"),
    # path('view_user/', user_view, name='view_user'),
    # path('list_user/', user_list, name='list_user'),
    # path('view_specific_user/<int:id>', view_specific_user, name='view_specific_user'),
]
